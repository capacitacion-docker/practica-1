# Practica 1 Dockerfile/Docker Hub

## Pre-requisitos

## Instalar Docker 


https://docs.docker.com/engine/install/

---------------------------------------------------------------
## Opcional - Modificar red virtual de Docker

Por defecto docker utiliza la red 172.17.0.xx, tanto para la interfaz virtual docker0 como para las redes virtuales por donde se comunicaran los contenedores. 

En caso de que en nuestro lugar de trabajo se utilicen ips en ese rango de red, lo recomendable es modificar el archivo daemon.json que es donde se definen parametros de configuracion de docker, entre ellos los de red.

El archivo daemon.json se encuentra en /etc/docker/daemon.json para sistemas linux o en C:\ProgramData\docker\config\daemon.json para Windows.

Si el archivo no existe, lo creamos y agregamos lo siguiente:

```json
{
 "bip": "10.20.10.5/24",
 "default-address-pools": [
  {
    "base": "10.30.0.0/16",
    "size": 24
  }
  ]
}
```
Por ultimo reiniciamos el servicio

> sudo systemctl restart docker.service

importante: Las direcciones ip utilizadas son a modo de ejemplo, consultar con el area de redes sobre que direcciones pueden utilizarse sin generar conflictos en la intranet.

---------------------------------------------------------------

## Practica
---------------------------------------------------------------
**Crear directorio de trabajo**

Creamos un directorio de trabajo donde nos quede comodo, por ejemplo en /home/user/ crearemos el directorio "proyectos".


**Clonar el repositorio de Gitlab con el proyecto de practica:**

En el directorio de trabajo creado, clonaremos el repositorio con el proyecto.

> git clone https://gitlab.com/capacitacion-docker/practica-1.git

**Nos movemos al directorio del proyecto**

> cd practica-1/

**Modificamos el archivo index.html**

Editaremos el archivo index.html, modificando la linea "Practica 1: XXXXX XXXXXX" por el texto que mostrara el servidor web al iniciar la aplicacion.

**Build de la imagen**

> docker build -t imagen-practica1 .


**Listamos las imagenes**

> docker images

**Comando para iniciar el contenedor con la imagen de nginx:**

> docker run --name contenedor-practica1 -p 8080:80 practica1

**Probamos que la aplicacion haya iniciado correctamente:**

Para ello ingresamos a la direccion:

http://localhost:8080/
 

**Asignamos un tag a la imagen creada anteriormente**

> docker tag imagen-practica1 desarrolloundav/practica1:MI_TAG

MI_TAG debe reemplazarse por una etiqueta que sirva para diferenciar la version de la imagen, por ejemplo "cristian", "cristian_v1.0", etc.


**"Pusheamos" la imagen al repositorio de Docker Hub**

Antes de realizar el push, tendremos que logearnos con el siguiente comando:

> docker login

importante: solicitar que el usuario sea agregado como colaborador al repositorio, de lo contrario al querer subir la imagen dara error de acceso denegado.

> docker push desarrolloundav/practica1:cristian

