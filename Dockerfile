#Imagen base que vamos a utilizar
FROM php:7.4-apache

#Copiamos la configuracion del virtual host de apache
COPY 000-default.conf /etc/apache2/sites-available/000-default.conf

#Habilitamos los modulos de apache
RUN a2enmod rewrite

#Copiamos el codigo Fuente de la aplicacion
COPY ./app /var/www/

#Dar permisos al usuario www-data
RUN chown -R www-data:www-data /var/www

#ejecutamos apache
CMD apachectl -D FOREGROUND 
